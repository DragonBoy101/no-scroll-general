setStage(0);

function setStage(n) {

    document.querySelectorAll('.steps-section__number').forEach(element => {
        if (element.classList.contains('stage-' + n)) {
            element.classList.add('on');
        } else {
            element.classList.remove('on');
        }
    });

    document.querySelectorAll('.stage_text').forEach(element => {
        if (element.classList.contains('stage-' + n)) {
            element.classList.add('on');
        } else {
            element.classList.remove('on');
        }
    });

    // document.querySelector('.line').classList = "line stage-" + n;
  
    setTimeout(() => {
        setStage((n + 1) % 3)
    }, 4000);

}